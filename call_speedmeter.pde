velocimetro v=new velocimetro();
import ddf.minim.*;
Minim minim;
AudioPlayer player;

int acelerador=0; //Variable aceleracion inicializada en 0
int frenado=0; //variable frenado inicializada en 0
int tiempo=10000; //variable tiempo
void setup()
{
  size(1200,600);
  v.setSize(3);
  v.setColor(173,168,31);
  v.setColorText(76,212,234);
  v.setPaso(25);
  v.setMinMax(0,300);
  minim = new Minim(this);                        //uso de la funcion minim
player = minim.loadFile("alarma_5.mp3");
}
void draw()
{
 
background(0,50,50);  //FONDO GRISS
fill(255);   //color blanco para texto
textSize(50); //tamaño de la fuente
text("acelerador", 610,200);
textSize(50);        
text("frenado", 950,200);
  v.show();
}
